import { IUser } from '../data/users';

export class User implements IUser{
    name: string;
    picture: string;

    constructor(name: string, picture: string){
        this.name = name;
        this.picture = picture;
    }

}