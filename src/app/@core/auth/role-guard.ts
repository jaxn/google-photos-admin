import { Injectable } from '@angular/core';
import { 
  Router,
  CanActivate,
  ActivatedRouteSnapshot
} from '@angular/router';
import { IAuthService } from './auth-service';

@Injectable()
export class RoleGuard implements CanActivate {
  constructor(public auth: IAuthService, public router: Router) {}
  canActivate(route: ActivatedRouteSnapshot): boolean {
    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;

    console.log('RoleGuardService.canActivate. Expected role: ' + expectedRole);

    const hasRole = this.auth.isAuthenticated() && this.auth.isInRole(expectedRole);
  
    if (hasRole){
      console.log('Guard returning true')
      return true;
    } else {
      console.log('Guard returning false')
      this.router.navigate(['pages/auth/login']);
      return false;
    }
    
  }
}