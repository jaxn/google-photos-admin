import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthResult } from './auth-result';

@Injectable()
export abstract class IAuthService {

  public abstract onAuthentictionChanged(): Observable<AuthResult>;
  public abstract isInRole(role: string);
  public abstract isAuthenticated(): boolean
  public abstract requestLogin();
  public abstract handleLoginCallback();
  public abstract logout();

}
