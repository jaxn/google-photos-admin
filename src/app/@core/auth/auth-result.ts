import { NbAuthResult, NbAuthOAuth2Token } from '@nebular/auth';
import { IUser } from '../data/users';
import { User } from './user';


export class AuthResult {

  constructor(private nbAuthResult: NbAuthResult) { }

  public IsAuthenticated(): boolean {
    get: {
      console.log('in IsAuthenticated')

      const tokenIsPresent = this.Token() ? true : false;
      const nbAuthResultSuccess = this.nbAuthResult?.isSuccess();
      const tokenIsValid =  this.nbAuthResult?.getToken()?.isValid();

      console.log(`tokenIsPresent: ${tokenIsPresent}, nbAuthResultSuccess: ${nbAuthResultSuccess}, tokenIsValid: ${tokenIsValid}`)
      
      return tokenIsPresent && tokenIsValid && nbAuthResultSuccess;
    }

  }

  public GetUser(): IUser {
    get: {
      return new User('test', 'assets/images/nick.png')
    }
  }
  
  public getRedirectUrl(): string {
    get: {
      return this.nbAuthResult?.getRedirect() ?? '';
    }
  }

  public Token(): NbAuthOAuth2Token {
    get: {
      return this.nbAuthResult?.getToken() as NbAuthOAuth2Token;
    }
  }

}