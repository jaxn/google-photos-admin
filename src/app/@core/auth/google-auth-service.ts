import { Injectable } from '@angular/core';
import { NbAuthResult, NbAuthService } from '@nebular/auth';
import { takeUntil } from 'rxjs/operators';
import { Observable, Subject, BehaviorSubject } from 'rxjs';
import { IAuthService } from './auth-service';
import { AuthResult } from './auth-result';

@Injectable()
export class GoogleAuthService implements IAuthService {

    private destroy$ = new Subject<void>();
    private strategyName : string = 'google';
   
    private result$ = new BehaviorSubject<AuthResult>(new AuthResult(null));

    constructor(private nbAuthService : NbAuthService) {}

    isInRole(role: string) {
      console.log('is in role ' + role)
      return this.isAuthenticated();
    }

    isAuthenticated(): boolean {
      return this.result$?.value?.IsAuthenticated() ?? false;
    }

    onAuthentictionChanged(): Observable<AuthResult> {
      return this.result$.asObservable();
    }

    setAuthenticationState(result: AuthResult) {
      this.result$.next(result);
    }
      
    requestLogin() {
      this.nbAuthService.authenticate(this.strategyName)
        .pipe(takeUntil(this.destroy$))
        .subscribe((authResult: NbAuthResult) => {
          //Do not handle a response.
        });
    }
    
    handleLoginCallback() {
        console.log('in handleLoginCallback');
        this.nbAuthService.authenticate(this.strategyName)
          .pipe(takeUntil(this.destroy$))
          .subscribe((authResult: NbAuthResult) => {
            console.log('in handleLoginCallback - callback. ' + JSON.stringify(authResult)); 
            this.setAuthenticationState(new AuthResult(authResult));
          });
    }
  
    logout() {
          this.nbAuthService.logout(this.strategyName)
            .pipe(takeUntil(this.destroy$))
          .subscribe((authResult: NbAuthResult) => {
            this.setAuthenticationState(new AuthResult(authResult));
          });
      }
}