import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbAuthModule, NbOAuth2AuthStrategy, NbOAuth2ResponseType } from '@nebular/auth';
import { NbSecurityModule, NbRoleProvider } from '@nebular/security';
import { of as observableOf } from 'rxjs';

import { throwIfAlreadyLoaded } from './module-import-guard';
import {
  LayoutService,
  StateService,
} from './utils';

import { UserData } from './data/users';
import { SmartTableData } from './data/smart-table';
import { StatsProgressBarData } from './data/stats-progress-bar';

import { UserService } from './mock/users.service';
import { SmartTableService } from './mock/smart-table.service';

import { StatsProgressBarService } from './mock/stats-progress-bar.service';
import { GoogleAuthService } from './auth/google-auth-service';

import { MockDataModule } from './mock/mock-data.module';
import { IAuthService } from './auth';
import { RoleGuard } from './auth/role-guard';

const DATA_SERVICES = [
  { provide: UserData, useClass: UserService },
  { provide: SmartTableData, useClass: SmartTableService },
  { provide: StatsProgressBarData, useClass: StatsProgressBarService }
];

const CORE_SERVICES = [
  { provide: IAuthService, useClass: GoogleAuthService },
  RoleGuard
];

export class NbSimpleRoleProvider extends NbRoleProvider {
  getRole() {
    // here you could provide any role based on any auth flow
    return observableOf('guest');
  }
}

export const NB_CORE_PROVIDERS = [
  ...MockDataModule.forRoot().providers,
  ...CORE_SERVICES,
  ...DATA_SERVICES,
  NbSecurityModule.forRoot({
    accessControl: {
      guest: {
        view: '*',
      },
      user: {
        parent: 'guest',
        create: '*',
        edit: '*',
        remove: '*',
      },
    },
  }).providers,
  {
    provide: NbRoleProvider, useClass: NbSimpleRoleProvider,
  },
  LayoutService,
  StateService,

];

@NgModule({
  imports: [
    CommonModule,
    NbAuthModule.forRoot({
      strategies: [
        NbOAuth2AuthStrategy.setup({
          name: 'google',
          clientId: '902550034146-68264vjcvem3c9oldb8b6lgh5mn056km.apps.googleusercontent.com',
          clientSecret: 'Z2MEvz3vrKRSria20gHtSKrZ',
          authorize: {
            endpoint: 'https://accounts.google.com/o/oauth2/v2/auth',
            responseType: NbOAuth2ResponseType.TOKEN,
            scope: 'https://www.googleapis.com/auth/userinfo.profile',
            redirectUri: 'http://localhost:4200/pages/auth/callback'
            
          },
        }),
      ],
    }),
  ],
  exports: [
    NbAuthModule,
  ],
  declarations: [],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: [
        ...NB_CORE_PROVIDERS,
      ],
    };
  }
};
