import { Observable } from 'rxjs';

export interface IUser {
  name: string;
  picture: string;
}

export interface Contacts {
  user: IUser;
  type: string;
}

export interface RecentUsers extends Contacts {
  time: number;
}

export abstract class UserData {
  abstract getUsers(): Observable<IUser[]>;
  abstract getContacts(): Observable<Contacts[]>;
  abstract getRecentUsers(): Observable<RecentUsers[]>;
}
