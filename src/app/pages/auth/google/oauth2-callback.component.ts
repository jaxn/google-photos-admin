import { Component, OnDestroy } from '@angular/core';
import { NbAuthResult, NbAuthService } from '@nebular/auth';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IAuthService } from 'app/@core/auth';
import { AuthResult } from 'app/@core/auth/auth-result';

@Component({
  selector: 'nb-playground-oauth2-callback',
  template: `
    <nb-layout>
      <nb-layout-column>Authenticating...</nb-layout-column>
    </nb-layout>
  `,
})
export class OAuth2CallbackComponent implements OnDestroy {

  private destroy$ = new Subject<void>();

  constructor(private authService: IAuthService, private router: Router) {

    this.authService.onAuthentictionChanged()
    .pipe(takeUntil(this.destroy$))
    .subscribe((authResult: AuthResult) => {
      console.log('in onAuthentictionChanged callback')

      const redirectUrl = authResult.getRedirectUrl();

      console.log(`callback url: ${redirectUrl}`)
      console.log(`authResult.IsAuthenticated(): ${redirectUrl}`)

      if (authResult.IsAuthenticated() && redirectUrl) {
        console.log(`navigating to: ${redirectUrl}`)
        //this.router.navigateByUrl(redirectUrl);
        this.router.navigateByUrl('/pages/tables');
      }
    });

    this.authService.handleLoginCallback();
   
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}