import { Component, OnDestroy } from '@angular/core';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { IAuthService } from 'app/@core/auth';

@Component({
    selector: 'nb-oauth2-login',
    template: `
      <button class="btn btn-success" *ngIf="!isAuthenticated" (click)="login()">Sign In with Google</button>
    `,
  })
  export class OAuth2LoginComponent implements OnDestroy {
    
    private destroy$ = new Subject<void>();

    constructor(private authService: IAuthService) {
      this.login();
    }

    isAuthenticated(){
      return this.authService.isAuthenticated();
    }
  
    login() {
      this.authService.requestLogin();
      //Result will comeback to the callback component
    }
 
    ngOnDestroy(): void {
      this.destroy$.next();
      this.destroy$.complete();
    }
  }