import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { RoleGuard } from 'app/@core/auth/role-guard';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'auth', 
      loadChildren: () => import('./auth/google/oauth2.module')
        .then(m => m.OAuth2Module),
    },
    {
      path: 'tables',
      canActivate: [RoleGuard],
      data: {
        expectedRole: 'administrator'
      }, 
      loadChildren: () => import('./tables/tables.module')
        .then(m => m.TablesModule),
    },   
    {
      path: '',
      redirectTo: '',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
